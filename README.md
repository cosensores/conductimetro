# CONDUCTímetro
**Herramienta libre para la medición de conductividad (μS/cm) en soluciones acuosas.**

* Adaptación del codigo de [DFRobot Gravity: Analog TDS Sensor/Meter](https://www.dfrobot.com/wiki/index.php/Gravity:_Analog_TDS_Sensor_/_Meter_For_Arduino_SKU:_SEN0244)

* Librerias control sensor temperatura (ds18b20): 
    1. [One wire](https://github.com/PaulStoffregen/OneWire)
    2. [DallasTemperature](https://github.com/milesburton/Arduino-Temperature-Control-Library)

A continuación se presenta una guía de armado y operación del equipo. *Para más información se recomienda leer el [Manual de armado y operación](Manual de armado y operación.pdf)*

# Conductividad

La conductividad eléctrica (μS/cm) es la medida de la capacidad del agua para conducir la electricidad. El agua pura prácticamente no conduce la electricidad, por lo tanto, la conductividad que podamos medir será indicativa de las sales disueltas y la materia ionizable presentes en el agua. 

Davis y De Weist clasifican las aguas superficiales en 4 tipos según su conductividad:
> * Dulce (Fresh) 0-1000 ppm
> * Salobre (Brackish) 1000-10000 ppm
> * Salina (Saline) 10000-100000 ppm
> * Salmuera (Brine) >100000 ppm
> 
> *(Davis SN, De Weist RJ, 1966; Hydrogeology. Wiley, New York, p. 463)*

# Armado
A continuación se presentan los módulos necesarios y el esquema de conexión:

1. DFRduino UNO R3
2. Analog TDS Sensor / Meter Module
3. Sensor de conductividad
4. Sensor de temperatura Ds18b20
5. Placa bluetooth HC-05

![foto](Imagenes/Conexion.png)

Se recomienda disponer las placas DFRduino UNO R3, Analog TDS Sensor / Meter Module y Bluetooth HC-05 con sus respectivos cables de conexión dentro de una caja aislada, que cuente con una perforación de entrada para la conexión del Power Bank y una perforación de salida para las sondas de conductividad y temperatura, con el fin de garantizar que las mismas no entren en contacto con agua, químicos o suciedad que pudieran dañarlas. 

![foto](/Imagenes/conductimetro2.jpg)

![foto](/Imagenes/conductimetro1.jpg)

# Funcionamiento
Para poner en funcionamiento el equipo se deberán seguir los siguientes pasos:

## _1. Conectar el Power Bank a la placa DFRduino UNO R3._
Se recomienda la utilización de un Power Bank (o cargador portátil) comercial como fuente de alimentación del equipo. Sin embargo, se pueden utilizar otras fuentes cuyos voltajes se mantengan en el rango de 5-12V con el fin de no dañar las placas y sondas del conductímetro.

## _2. Conectar mediante bluetooth el equipo con la aplicación CONDUCTímetro en un dispositivo Android._ 

![foto](Imagenes/Conexion a bluetooth.png)
Los datos de conductividad y temperatura deberán aparecer en la pantalla luego de unos segundos.

## _3. Calibrar el equipo._

![foto](Imagenes/Calibrado.png)

## _4. Sumergir las sondas de temperatura y conductividad en la muestra a analizar._

Se recomienda no sumergir las sondas completamente. Además, para una correcta recolección de datos se recomienda que las sondas no estén en contacto entre sí ni con las paredes del recipiente que contenga las muestras. 

![foto](Imagenes/Toma de muestras.jpg)

# Caracterización del equipo
El límite de cuantificación (LOQ) del equipo es de: 29 μS/cm.
El rango de linealidad es de 29 a 1080 μS/cm.

Los valores de conductividad son calculados a partir del voltaje registrado por la sonda según la siguiente ecuación:

Conductividad = 689,75 x Voltaje

![foto](Imagenes/Conductividad vs voltaje.png)
