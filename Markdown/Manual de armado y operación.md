<script
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
  type="text/javascript">
</script>


<br>
<br>
<br>
<br>

<h1 style="text-align: center;">Manual de armado y operación de <br> un conductímetro basado en <br>Arduino</h1>


<br>
<br>
<br>
<br>

![](pag1.jpg)

<div style="page-break-after: always"></div>

## Índice
  
- [Índice](#índice)
- [1. Introducción](#1-introducción)
- [2. Conductímetro](#2-conductímetro)
- [Armado](#armado)
    - [Power Bank](#power-bank)
  - [Caracterización del equipo](#caracterización-del-equipo)
  - [Utilización](#utilización)
    - [Conexión a dispositivo Android](#conexión-a-dispositivo-android)
    - [Calibración](#calibración)
      - [Preparación de estándares de calibración](#preparación-de-estándares-de-calibración)
      - [Modo calibración de la aplicación CONDUCTímetro](#modo-calibración-de-la-aplicación-conductímetro)
    - [Toma de datos](#toma-de-datos)
- [Solución de problemas](#solución-de-problemas)
- [Referencias](#referencias)
- [Anexos](#anexos)
  - [Definiciones](#definiciones)

<div style="page-break-after: always"></div>
<a name="item1"></a>

## 1. Introducción


La conductividad eléctrica es la medida de la capacidad del agua para conducir la electricidad. El agua pura prácticamente no conduce la electricidad, por lo tanto, la conductividad que podamos medir será indicativa de las sales disueltas y la materia ionizable presentes en el agua.

Las unidades normalmente utilizadas para la conductividad son µs/cm. Además, dado que las sales se disocian en agua como cationes y aniones, la medición de la conductividad es una medición indirecta los sólidos disueltos totales ⦍ppm TDS⦎. Cuanto mayor sea la cantidad de sólidos disueltos en agua, mayor será su conductividad. En particular, el pasaje de unidades aproximado de conductividad a salinidad se puede calcular según (Chacón Chaquea, M., 2016):

$$
\frac \mu {cm} = 2ppmTDS
$$

Davis y De Weist clasifican las aguas superficiales en 4 tipos según su conductividad (Davis SN, 1966):

 - Dulce (Fresh) *0-1000 ppm*
 - Salobre (Brackish) *1000-10000 ppm*
 - Salina (Saline) *10000-100000 ppm*
 - Salmuera (Brine) *>100000 ppm*


<br>

![](fig1.jpg)

**Figura 1.** Clasificación del agua según su concentración de sólidos disueltos totales (TDS) en partes por millón (ppm). Fuente: Davis SN, De Weist RJ (1966).

El nivel guía máximo según comunidad europea es *2500 μS/cm (20°C)* (Directiva 98/83/EC, 1998) y recomendación de la Agencia de Protección Ambiental de EEUU (EPA 2013) *500 ppm*.

En el Anexo A del de la ley *26.221*, se establecen las normas mínimas de calidad de agua producida y distribuida a cargo de AySA en el Área Metropolitana de Buenos Aires, y dentro de ellas se fija un valor para el residuo conductimétrico de *1000mg/L*.

Por otro lado, un conductímetro permite medir la capacidad de una solución para transportar una corriente eléctrica, que depende de la presencia de especies cargadas. En la conducción de la corriente eléctrica en una solución con especies cargadas, los cationes migran hacia un cátodo, es decir, el electrodo negativo de un circuito; y los aniones hacia el ánodo, es decir, el electrodo positivo.
<div style="page-break-after: always"></div>

<a name="item2"></a>

## 2. Conductímetro

En el presente manual se detallará las condiciones de calibración y uso óptimas de un conductímetro desarrollado a base de Arduino.

Para construir el conductímetro basado en Arduino se requiere los siguientes equipos:

 1. DFRduino UNO R3 
 2. Analog TDS Sensor / Meter Module
 3. Sensor de conductividad 
 4. Sensor de temperatura Ds18b20
 5. Placa bluetooth HC-05
 6. Power Bank *5* a *12 V*
 7. Teléfono celular o equipo con Android
 

A continuación, se presenta un esquema general del equipo armado:

![](fig2.jpg)

**Figura 2.** Esquema general del conductímetro.

Para poner en funcionamiento el equipo se deberán seguir los siguientes pasos:

1. Conectar el Power Bank a la placa DFRduino UNO R3.
2. Conectar mediante bluetooth el equipo con la aplicación CONDUCTímetro en el dispositivo Android. Los datos de conductividad y temperatura deberán aparecer en la pantalla luego de unos segundos. 
3. Calibrar el equipo.
4. Sumergir las sondas de temperatura y conductividad en la muestra a analizar.

Instrucciones más precisas y recomendaciones generales sobre el armado y puesta en marcha del equipo se presentan en las siguientes subsecciones del manual.

<div style="page-break-after: always"></div>

<a name="item3"></a>

## Armado

La conexión de las placas DFRduino UNO R3, Analog TDS Sensor / Meter Module y
Bluetooth HC-05 y el Sensor de temperatura Ds18b20 se muestra en la figura 3.

![](fig3.jpg)

**Figura 3.** Esquema de conexión de las placas DFRduino UNO R3, Analog TDS Sensor / Meter Module y Bluetooth HC-05 y el Sensor de temperatura Ds18b20.

Se recomienda disponer las placas DFRduino UNO R3, Analog TDS Sensor / Meter Module y Bluetooth HC-05 con sus respectivos cables de conexión dentro de una caja aislada, que cuente con una perforación de entrada para la conexión del Power Bank y una perforación de salida para las sondas de conductividad y temperatura, con el fin de garantizar que las mismas no entren en contacto con agua, químicos o suciedad que pudieran dañarlas.

El código de Arduino, que debe ser cargado a la placa DFRduino UNO R3, como así también la aplicación CONDUCTímetro se encuentran disponibles en GitLab.

<div style="page-break-after: always"></div>

<a name="item4"></a>

#### Power Bank

Se recomienda la utilización de un Power Bank (o cargador portátil) comercial como fuente de alimentación del equipo. Sin embargo, se pueden utilizar otras fuentes cuyos voltajes se mantengan en el rango de 5-12V con el fin de no dañar las placas y sondas del conductímetro.

<a name="item5"></a>

### Caracterización del equipo

 - El límite de cuantificación (LOQ) del equipo es de: 
$$\frac {\mu S}  {cm}$$
 - El rango de linealidad es de *29*  a:
$$1080 \frac {\mu S}  {cm}$$

Los valores de conductividad son calculados a partir del voltaje registrado por la sonda según la siguiente ecuación:

$$
Conductividad = 689,75\pm5,36  \times Voltaje 
$$

![](fig4.jpg)


**Figura 4.** Conductividad en función del voltaje. Pendiente de la recta: 

$$689,75 \pm 5,36$$

<a name="item6"></a>

### Utilización

<a name="item7"></a>

#### Conexión a dispositivo Android

Para conectar el equipo con la aplicación CONDUCTímetro, se deben seguir los pasos que se presentan en la Figura 5.

![fig](fig5.jpg){ width="800" height="600" style="display: block; margin: 0 auto" }
 

**Figura 5.** Instrucciones de conexión mediante bluetooth del equipo a la aplicación CONDUCTímetro.

1. Verificar que la placa HC-05 se encuentre vinculada mediante bluetooth con el dispositivo Android.
2. Entrar en la aplicación CONDUCTímetro y hacer clic en el logo de bluetooth.
3. Hacer clic en la opción que presente la placa HC-05. Los números previos pueden variar.

Luego de la conexión por Bluetooth los datos de conductividad y temperatura deberían comenzar a aparecer en la pantalla.

<a name="item8"></a>

#### Calibración 

<a name="item9"></a>

##### Preparación de estándares de calibración
En general, se utilizan soluciones de cloruro de potasio como estándares de calibración de equipos de medición de conductividad. A continuación, se presentan los procedimientos recomendados por distintos organismos (ANMAT y ASTM) para la generación de soluciones de cloruro de potasio con conductividad conocida.

 - Solución estándar *A*: Preparar una solución que contenga *74,2460 g* de cloruro de potasio por cada litro de solución a *20 °C*, empleando agua libre de dióxido de carbono, preparada con agua cuya conductividad no excede de *2 \mu S/cm*.
 - Solución estándar *B*: Preparar una solución que contenga *7,4365 g* de cloruro de potasio por cada litro de solución a *20 °C*, empleando agua libre de dióxido de carbono, preparada con agua cuya conductividad no excede de *2 \mu S/cm*.
 - Solución estándar *C*: Preparar una solución que contenga *0,7440 g* de cloruro de potasio por cada litro de solución a *20 °C*, empleando agua libre de dióxido de carbono, preparada con agua cuya conductividad no excede de *2 \mu S/cm*.
 - Solución estándar *D*: Diluir *100 ml* de la Solución estándar *C* a $1$ litro a *20 °C*

La conductividad de las cuatro soluciones estándar de cloruro de potasio, a las temperaturas de *0 °C, 18 °C* y *25 °C* se indican en la Tabla 1.


Tabla 1. Soluciones estándar recomendadas por ASTM y ANMAT con su conductividad correspondiente.

<table>
<tr>  <th>Solución estándar</th>  <th>Normalidad de la solución</th>  <th>Temperatura (°C)</th> <th>Conductividad (μS/cm)</th> </tr>
<tr>  <td rowspan="3">A (ASTM)</td>  <td rowspan="3">1</td>  <td>0</td>  <td>65176 </td> </tr>
<tr>  <td>18</td>  <td>97838 </td> </tr>
<tr>  <td>25</td>  <td>111342 </td> </tr>
<tr>  <td rowspan="3">B (ASTM)</td>  <td rowspan="3">0,1</td>  <td>0</td>  <td>7138 </td> </tr>
<tr>  <td>18</td>  <td>11157</td> </tr>
<tr>  <td>25</td>  <td>12856</td> </tr>
<tr>  <td rowspan="3">B (ASTM)</td>  <td rowspan="3">0,1</td>  <td>0</td>  <td>77,69 </td> </tr>
<tr>  <td>18</td>  <td>127,54</td> </tr>
<tr>  <td>25</td>  <td>146,93</td> </tr>
</table>

Por otra parte, se pueden adquirir patrones de conductividad comerciales. Se recomienda preparar o adquirir estándares que posean conductividad similar a la esperada en la muestra en estudio.

<a name="item10"></a>

##### Modo calibración de la aplicación CONDUCTímetro
Los pasos a seguir para calibrar el equipo desde la aplicación CONDUCTímetro (ver Figura
6) son:
1. Haga clic en el botón “calibrar”.
2. Ingrese el valor de conductividad (en unidades de µs/cm) en el espacio disponible de la aplicación. Haga clic en el botón:
 $$\frac{\mu s}{cm}$$ 
Verifique que aparece en pantalla la
leyenda <calibrando>.
1. Espere hasta que aparezca en pantalla la leyenda "calibrado".
2. Haga clic en el botón “salir”. Espere que aparezca en pantalla la leyenda <calibrado, salir>.

![](fig7.jpg)

**Figura 6.** Instrucciones de calibrado del equipo mediante la aplicación CONDUCTímetro.

<a name="item11"></a>

#### Toma de datos
Con el fin de no poner en contacto los circuitos internos de las sondas de conductividad y temperatura, se recomienda no sumergirlos completamente. Además, para una correcta recolección de datos se recomienda que las sondas no estén en contacto entre sí ni con las paredes del recipiente que contenga las muestras (Ver Figura 7).

![](fig8.jpg)

**Figura 7:** Correcta disposición de las sondas en el recipiente que contiene la muestra. Se recomienda que las sondas no estén en contacto entre sí ni con las paredes del recipiente.

Luego de verificar tener correctamente dispuestas las sondas en el recipiente que contiene la muestra, se recomienda esperar que se muestren en pantalla al menos 5 valores de conductividad hasta recolectar el dato final, con el fin de permitir que el equipo se estabilice.

Nota:
 - El límite de cuantificación (LOQ) del equipo es de: 
  $$29 \mu S/cm$$ 
 - El rango de linealidad es de *29* a:
 $$1080 \mu S/cm$$

 

<div style="page-break-after: always"></div>


<a name="item12"></a>

## Solución de problemas
**Tabla 2.** Problemas comunes con sus posibles causas y soluciones.

<table>
<tr>  <th>Problema</th>  <th>Posible causa</th>  <th>Solución</th> </tr>
<tr>  <td rowspan="2">La aplicación CONDUCTímetro no conecta con la placa HC-05.</td><td>Incorrecta conexión de la placa.</td>  <td>Verifique que la placa HC-05 se encuentre conectada de acuerdo a lo mostrado en la Figura 3. Una vez conectado el Power Bank la placa enciende una luz roja.</td></tr>
<tr>  <td>El dispositivo Android no reconoce la placa.</td>  <td>Si tenía la placa HC-05 vinculada con su dispositivo Android, desvincule la misma y vuelva a conectarla. </td> </tr>
<tr> <td>No es posible entrar en el modo calibración</td> <td>Errores asociados al código Arduino.</td>  <td>Revise que ha cargado en el equipo la última versión disponible del código Arduino para el conductímetro.</td> </tr>
<tr> <td rowspan="3">Fluctuación inesperada de la conductividad de las muestras.</td><td>Cables en malas condiciones.</td>  <td>Verifique que los cables utilizados en la conexión se encuentren en buenas condiciones.</td></tr>
<tr> <td>Incorrecta disposición de las sondas</td>  <td>Disponga las sondas como se muestra en la Figura 7.</td></tr>
<tr> <td>Fluctuación de la fuente de alimentación</td>  <td>Asegure conectar la placa Arduino a una fuente que entrega voltaje considerablemente constante. Se recomienda la utilización de Power Bank (o cargador portátil) o de una computadora.</td></tr>
<tr> <td>Pobre homogeneidad de las muestras.</td> <td>Contaminación de los patrones.</td>  <td>Prepare o adquiera nuevos patrones y verifique que la conductividad medida sea la esperada. En la preparación y manipulación de los patrones, asegure un adecuado lavado de los elementos utilizados.</td> </tr>
<tr> <td rowspan="5">Baja reproducibilidad.</td><td>Pobre homogeneidad de las muestras.</td>  <td>Mezclas las muestras a analizar previamente a la toma de datos. No utilizar las sondas para realizar el agitado.</td></tr>
<tr> <td>Falta de calibración del equipo.</td>  <td>Realizar una correcta calibración del equipo antes de realizar las mediciones.</td><td></td></tr>
<tr> <td>Las sondas se encuentran sucias.</td>  <td>Asegure una correcta limpieza de las sondas antes y luego de cada medida.</td></tr>
<tr> <td>Errores asociados al código Arduino.</td>  <td>Revise que ha cargado en el equipo a última versión disponible del código Arduino para el conductímetro.</td></tr>
<tr> <td>Contaminación de la muestra.</td>  <td>En la manipulación de las muestras, asegure un adecuado lavado de los elementos utilizados. En una serie de muestras a analizar, una práctica recomendada es realizar las mediciones en orden de conductividad creciente.</td></tr>
</table>

<a name="item13"></a>

## Referencias
ASTM. (2009). Standard test methods for electrical conductivity and resistivity of water.

Chacón Chaquea, M. (2016). Análisis físico y químico de la calidad del agua. Bogotá, Colombia: Ediciones USTA. Recuperado de: [https://elibro.net/es/ereader/unsam/68990?page=26](https://elibro.net/es/ereader/unsam/68990?page=26)

Davis SN, De Weist RJ (1966) Hydrogeology. Wiley, New York, p 463 

Directiva 98/83/EC (1998). Calidad del agua destinada a consumo humano. Recuperado de: [https://www.boe.es/doue/1998/330/L00032-00054.pdf ](https://www.boe.es/doue/1998/330/L00032-00054.pdf )




ANMAT (2014). Farmacopea Argentina, 7ª Edición compilada. Recuperado de: [http://www.anmat.gov.ar/webanmat/fna/pfds/libro_cuarto.pdf](http://www.anmat.gov.ar/webanmat/fna/pfds/libro_cuarto.pdf)


Ley 26.221 (2007). Anexo A. Normas mínimas de calidad de agua producida y distribuida. Recuperado de: [https://www.argentina.gob.ar/sites/default/files/anexo_anormas.pdf](https://www.argentina.gob.ar/sites/default/files/anexo_anormas.pdf)



<a name="item14"></a>

## Anexos
### Definiciones

- **Anión**: Ion con carga negativa.
 - **Catión**: Ion con carga positiva.
 - **Electrodo**: Extremo de un conductor en contacto con un medio, al que lleva o del que recibe una corriente eléctrica.
 - **Placa Arduino**: Placa de Circuito Impreso (PCB) que implementa un determinado diseño de circuitería interna predeterminado.


<div style="page-break-after: always"></div>



