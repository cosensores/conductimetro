/****************************************************

CONDUCTImtero - CoSensores (Sensores Comunitarios) 

https://gitlab.com/cosensores
https://www.facebook.com/cosensores/
https://www.instagram.com/cosensores/

Somos miembros de Universidades Nacionales trabajando junto a comunidades organizadas 
en el desarrollo de métodos para evaluar la presencia de contaminantes 
de manera sencilla en el territorio, acompañando acciones y procesos reivindicativos.

*****************************************************
Adaptación del codigo de DFRobot Gravity: Analog TDS Sensor/Meter 
https://www.dfrobot.com/wiki/index.php/Gravity:_Analog_TDS_Sensor_/_Meter_For_Arduino_SKU:_SEN0244

 1. El codigo fue evaluado en Arduino Uno - IDE 1.0.5
 2. Para calibrar:
     CALIBRAR -> ingresa al modo calibracio'n 
     CAL:"valor de conductividad del patro'n" -> se recomienda sc 692 mg/L NaCl = 1413 uS/cm a 25^c 
     SALIR -> guarda los parametros y sale del modo de calibracio'n
     
****************************************************
Librerias control sensor temperatura: ds18b20
https://github.com/PaulStoffregen/OneWire
https://github.com/milesburton/Arduino-Temperature-Control-Library

****************************************************/


/***************************** Definiciones de variables y librerrías ************************************/
//temperatura
#include <OneWire.h>                
#include <DallasTemperature.h>
OneWire ourWire(3);                //conectar sensor temperatura a pin digital 2
DallasTemperature sensors(&ourWire); //declara una variable u objeto para nuestro sensor

//conductividad
#include <EEPROM.h>
#include "GravityTDS.h"
#define TdsSensorPin A4             //conectar sensor conductividad en pin A4
GravityTDS gravityTds;
#define SCOUNT  10           // sum of sample point
int analogBuffer[SCOUNT];    //store the sample voltage
int analogBufferIndex = 0;

float temperature = 25, tdsValue = 0, kValue = 0, ecValue25 = 0, ecValue = 0, voltage = 0, voltageM = 0;

/***************************** Se recoge información y se declaran funciones ************************************/

void setup()
{ 
Serial.begin(9600); //Le indica al Arduino que inicie comunicación con cualquier dispositivo conectado a los pines RX y TX

//tempratura
sensors.begin();  //Se inicia el sensor

//conductividad
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(5.0);       //voltage de referencia en ADC, default 5.0V en Arduino UNO
    gravityTds.setAdcRange(1024);  //1024 para 10bit ADC;4096 para 12bit ADC
    gravityTds.begin();            //Se inicia el sensor   
}

/************************************ Código a ejecutar ************************************************/

void loop()
{

//temperatura  
sensors.requestTemperatures();   //Se envía el comando para leer la temperatura
float temp= sensors.getTempCByIndex(0); //Se obtiene la temperatura en ºC
temperature = (temp-0.503)/0.941;       //temperatura corregida

//conductividad

kValue = gravityTds.getKvalue();          //valor de calibración
    gravityTds.setTemperature(temperature);  //define temperatura para compensacio'n del valor de conductividad
    gravityTds.update();                     //lee y calcula
   temperature = (temp-0.503)/0.941;          //comentar para no levantar temperatura
      voltageM = gravityTds.getVoltageValue();    //Llama a la funcion que lee el voltaje
      ecValue = gravityTds.getEcValue();    //Llama a la funcion que calcula conductividad

 static unsigned long timepoint = millis();
    if(millis()-timepoint>3000U){                  //intervalo de tiempo
     timepoint = millis();
     analogBuffer[analogBufferIndex] = gravityTds.getEcValue25();    //lee el voltaje y lo guarda en el buffer cada 40ms
     analogBufferIndex++;
     if(analogBufferIndex == SCOUNT)
         analogBufferIndex = 0;
        ecValue25 = getMedianNum(analogBuffer,SCOUNT);   // obtiene un valor estable aplicando como filtro un valor medio


      }

// Serial.print(voltageM,3);
// Serial.println(" v");
 Serial.print(ecValue25,0);
 Serial.println(" uS/cm");
// Serial.print(temperature);
// Serial.println(" ^C");
// Serial.println(kValue,3);
    
    delay(3000);

}


int getMedianNum(int bArray[], int iFilterLen)
{
      int bTab[iFilterLen];
      for (byte i = 0; i<iFilterLen; i++)
      {
      bTab[i] = bArray[i];
      }
      int i, j, bTemp;
      for (j = 0; j < iFilterLen - 1; j++)
      {
      for (i = 0; i < iFilterLen - j - 1; i++)
          {
        if (bTab[i] > bTab[i + 1])
            {
        bTemp = bTab[i];
            bTab[i] = bTab[i + 1];
        bTab[i + 1] = bTemp;
         }
      }
      }
      if ((iFilterLen & 1) > 0)
    bTemp = bTab[(iFilterLen - 1) / 2];
      else
    bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
      return bTemp;
}
